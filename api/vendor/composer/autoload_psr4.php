<?php

// autoload_psr4.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'Drips\\Search\\' => array($baseDir . '/lib/search'),
    'Drips\\Image\\' => array($baseDir . '/lib/image'),
    'Drips\\Db\\' => array($baseDir . '/lib/db'),
    'Drips\\' => array($baseDir . '/lib'),
);
