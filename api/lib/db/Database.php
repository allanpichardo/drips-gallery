<?php
namespace Drips\Db;

/**
 * Class Database
 * NOTE: This class depends on a file db.json which contains the keys
 * db_host,
 * db_user,
 * db_password,
 * db_name
 * @package Drips\Db
 */
class Database {

    private $mysqli;
    private $connectionError = null;

    public function __construct() {

        try {
            $json = file_get_contents(dirname( __FILE__ ) . '/db.json');
            $keys = json_decode($json);
            $this -> mysqli = new \mysqli($keys->db_host, $keys->db_user, $keys->db_password, $keys->db_name) or die("Error connection to DB");
            //$this->mysqli = mysqli_connect(Database::$HOST, Database::$USER, Database::$PASSWORD, Database::$DATABASE) or die("Error " . mysqli_error($this->mysqli));
            //$this->mysqli = new mysqli(Database::$HOST, Database::$USER, Database::$PASSWORD, Database::$DATABASE);
        } catch (Exception $e) {
            $this->connectionError = $e->getMessage();
        }

    }

    public function query($query) {
        $result = mysqli_query($this->mysqli, $query);
        //$result = $this->mysqli->query($query);
        return $result;
    }
    
    public function isConnected(){
        return $this->connectionError != null;
    }

    public function getConnectionError() {
        return $this->connectionError;
    }

    public function getConnection() {
        return $this->mysqli;
    }

    /**
     * Returns the next available valid filename for
     * the given collection
     * @param type $collectionId
     */
    public function getNextFilename($collectionId){
        $query = "select count(file_name) as count from image where collection_id = %d";
        $query = sprintf($query,$collectionId);
        $result = query($query);
        if($result->num_rows > 0){
            //get prefix
            $query = "select prefix from collection where id = %s";
            $query = sprintf($query,$collectionId);
            $prefixRes = query($query);
            if($prefixRes->num_rows > 0){
                $numRow = $result->fetch_object();
                $prefixRow = $prefixRes->fetch_object();
                
                $imgCount = $numRow->count;
                $prefix = $prefixRow->prefix;
                
                //images/collection_{collectionId}/{prefix}{filenumber}.jpg
                
            }
        }
    }

}

?>