<?php
namespace Drips\Image;

use Drips\Db\Database;

class Image
{
    public function __construct(){

    }

    public static function get_random_image()
    {

        $exec = false;
        $image_url = "";

        $db = new Database();

        $query = "SELECT * FROM image WHERE collection_id != 2 AND width > 1300 ORDER BY RAND() LIMIT 1";
        $result = $db->query($query);
        if ($result->num_rows > 0) {
            $row = $result->fetch_object();
            $imageUrl = Image::get_base_url() . "/images/collection_" .
                $row->collection_id . "/" . $row->file_name;
            $image_url = $imageUrl;
            $exec = true;
        }

        $response = new \stdClass();
        $response->execution = $exec;
        $response->image_url = $image_url;
        $response = json_encode($response);
        return $response;
    }

    public static function get_random_image_lowres()
    {

        $exec = false;
        $image_url = "";

        $db = new Database();

        $query = "SELECT * FROM image WHERE collection_id != 2 ORDER BY RAND() LIMIT 1";
        $result = $db->query($query);
        if ($result->num_rows > 0) {
            $row = $result->fetch_object();
            $imageUrl = Image::get_base_url() . "/low_res_images/collection_" .
                $row->collection_id . "/" . $row->file_name;
            $image_url = $imageUrl;
            $exec = true;
        }

        $response = new \stdClass();
        $response->execution = $exec;
        $response->image_url = $image_url;
        $response = json_encode($response);
        return $response;
    }

    public static function get_base_url()
    {
        $url = isset($_SERVER['HTTPS']) ? 'https://' : 'http://';
        $url .= $_SERVER['SERVER_NAME'];
        $url .= htmlspecialchars($_SERVER['REQUEST_URI']);
        $baseurl = dirname(dirname($url));
        return $baseurl;
    }

    public static function compress_image($source_url, $destination_url, $quality)
    {
        $info = getimagesize($source_url);

        if ($info['mime'] == 'image/jpeg') $image = imagecreatefromjpeg($source_url);
        elseif ($info['mime'] == 'image/gif') $image = imagecreatefromgif($source_url);
        elseif ($info['mime'] == 'image/png') $image = imagecreatefrompng($source_url);

        //save file
        imagejpeg($image, $destination_url, $quality);

        //return destination file
        return $destination_url;
    }
}