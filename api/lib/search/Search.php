<?php
namespace Drips\Search;

use Drips\Db\Database;
use Drips\Image\Image;

class Search
{

    public static function get_collections()
    {
        $res = new \stdClass();
        $res->data = array();
        $res->execution = false;

        $db = new Database();

        $query = "SELECT * FROM collection order by name asc";
        $result = $db->query($query);
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_object()) {
                array_push($res->data, $row);
            }
            $res->execution = true;
        }

        return json_encode($res);
    }

    public static function get_locations()
    {
        $res = new \stdClass();
        $res->data = array();
        $res->execution = false;

        $db = new Database();

        $query = "SELECT * FROM location order by name asc";
        $result = $db->query($query);
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_object()) {
                array_push($res->data, $row);
            }
            $res->execution = true;
        }

        return json_encode($res);
    }

    public static function get_photographers()
    {
        $res = new \stdClass();
        $res->data = array();
        $res->execution = false;

        $db = new Database();

        $query = "SELECT * FROM photographer order by name asc";
        $result = $db->query($query);
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_object()) {
                array_push($res->data, $row);
            }
            $res->execution = true;
        }

        return json_encode($res);
    }

    public static function get_artists()
    {
        $res = new \stdClass();
        $res->data = array();
        $res->execution = false;

        $db = new Database();

        $query = "SELECT * FROM artist order by name asc";
        $result = $db->query($query);
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_object()) {
                array_push($res->data, $row);
            }
            $res->execution = true;
        }

        return json_encode($res);
    }

    public static function get_locales()
    {
        $res = new \stdClass();
        $res->data = array();
        $res->execution = false;

        $db = new Database();

        $query = "SELECT * FROM location order by name asc";
        $result = $db->query($query);
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_object()) {
                array_push($res->data, $row);
            }
            $res->execution = true;
        }

        return json_encode($res);
    }

    public static function get_styles()
    {
        $res = new \stdClass();
        $res->data = array();
        $res->execution = false;

        $db = new Database();

        $query = "SELECT * FROM graffiti_style order by style asc";
        $result = $db->query($query);
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_object()) {
                array_push($res->data, $row);
            }
            $res->execution = true;
        }

        return json_encode($res);
    }

    public static function get_mediums()
    {
        $res = new \stdClass();
        $res->data = array();
        $res->execution = false;

        $db = new Database();

        $query = "SELECT * FROM medium order by name asc";
        $result = $db->query($query);
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_object()) {
                array_push($res->data, $row);
            }
            $res->execution = true;
        }

        return json_encode($res);
    }

    public static function get_digital_specs()
    {
        $res = new \stdClass();
        $res->data = array();
        $res->execution = false;

        $db = new Database();

        $query = "SELECT digital_specs.*, born.origin FROM digital_specs LEFT JOIN born ON (digital_specs.born_id = born.id) order by equipment asc";
        $result = $db->query($query);
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_object()) {
                array_push($res->data, $row);
            }
            $res->execution = true;
        }

        return json_encode($res);
    }

    public static function browse_images($collectionId, $styleId, $mediumId, $originId, $page = 1, $artistQuery = "", $locationQuery = "", $photographerQuery = "", $lctgmQuery = "", $usernameQuery = "")
    {

        $page = ($page == 0) ? 1 : $page;

        $max = 10;
        $limit = " limit " . (($page - 1) * $max) . ", " . ($max);

        $res = new \stdClass();
        $res->execution = false;
        $res->data = array();
        $res->page = intval($page);

        $db = new Database();

        //base query
        $query = "SELECT SQL_CALC_FOUND_ROWS i.id AS image_id,i.date_taken, i.file_name, i.title, i.collection_id, c.name AS collection_name, group_concat(DISTINCT st.style SEPARATOR ', ') AS styles, group_concat(DISTINCT sp.equipment SEPARATOR ', ') AS equipment, sp.origin, group_concat(DISTINCT artists.name SEPARATOR ', ') AS artist_names, group_concat(DISTINCT artists.artist_id SEPARATOR ', ') AS artist_ids, location.name AS location_name, p.name AS photographer_name, lc.tgm AS lc_tgm, user.user_name, i.user_id FROM image i LEFT JOIN collection c ON (c.id = i.collection_id) LEFT JOIN (SELECT st.image_id, gs.style, gs.id AS style_id FROM relational_style st LEFT JOIN graffiti_style gs ON (st.style_id = gs.id)) st ON(st.image_id = i.id) LEFT JOIN (SELECT ds.image_id, sp.equipment, b.origin, b.id AS origin_id FROM relational_digital_specs ds LEFT JOIN digital_specs sp ON (ds.digital_specs_id = sp.id) LEFT JOIN born b ON (b.id = sp.born_id)) sp ON (sp.image_id = i.id) LEFT JOIN (SELECT ra.image_id, a.name, a.id AS artist_id FROM relational_artist ra LEFT JOIN artist a ON(ra.artist_id = a.id)) AS artists ON (artists.image_id = i.id) LEFT JOIN location ON (i.city_id = location.id) LEFT JOIN photographer p ON (p.id = i.photographer_id) LEFT JOIN lc_tgm lc ON (lc.id = i.lc_tgm_id) LEFT JOIN user ON (user.id = i.user_id) WHERE ";
        //figure out where clause
        //collections
        if ($collectionId == 0) {
            $query .= " i.collection_id > 0 ";
        } else {
            $query .= " i.collection_id = $collectionId ";
        }
        $query .= "and";
        //styles
        if ($styleId == 0) {
            $query .= " st.style_id > 0 ";
        } else {
            $query .= " st.style_id = $styleId ";
        }
        $query .= "and";
        //medium
        if ($mediumId == 0) {
            $query .= " i.medium_id > 0 ";
        } else {
            $query .= " i.medium_id = $mediumId ";
        }
        $query .= "and";
        //origin
        if ($originId == 0) {
            $query .= " sp.origin_id > 0 ";
        } else {
            $query .= " sp.origin_id = $originId ";
        }
        //artist
        if (strlen($artistQuery) > 0) {
            $query .= " and artists.name like ('%$artistQuery%') ";
        }
        if (strlen($locationQuery) > 0) {
            $query .= " and location.name like ('%$locationQuery%') ";
        }
        if (strlen($photographerQuery) > 0) {
            $query .= " and p.name like ('%$photographerQuery%') ";
        }
        if (strlen($lctgmQuery) > 0) {
            $query .= " and lc.tgm like ('%$lctgmQuery%') ";
        }
        if (strlen($usernameQuery) > 0) {
            $query .= " and user.user_name like ('%$usernameQuery%') ";
        }

        //group
        $query .= " group by file_name ";
        //limit
        $query .= $limit;

        //run the query
        $result = $db->query($query);
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_object()) {
                $imageUrl = Image::get_base_url() . "/images/collection_" .
                    $row->collection_id . "/" . $row->file_name;
                $thumbnailUrl = Image::get_base_url() . "/low_res_images/collection_" .
                    $row->collection_id . "/" . $row->file_name;
                $row->image_url = $imageUrl;
                $row->thumbnail_url = $thumbnailUrl;
                array_push($res->data, $row);
            }
            $res->execution = true;
        }

        //get pagination total
        $pageResult = $db->query("SELECT FOUND_ROWS() as total");
        if ($pageResult->num_rows > 0) {
            $row = $pageResult->fetch_object();
            $res->pages = ceil($row->total / $max);
        }

        return json_encode($res);
    }

    public static function browse_images_new($collectionQuery = "", $styleQuery = "", $mediumQuery = "", $artistQuery = "", $locationQuery = "", $photographerQuery = "", $usernameQuery = "", $originId, $page = 1)
    {

        $page = ($page == 0) ? 1 : $page;

        $max = 10;
        $limit = " limit " . (($page - 1) * $max) . ", " . ($max);

        $res = new \stdClass();
        $res->execution = false;
        $res->data = array();
        $res->page = intval($page);

        $db = new Database();

        //base query
        $query = "SELECT SQL_CALC_FOUND_ROWS i.id AS image_id,i.date_taken, i.file_name, i.title, i.collection_id, c.name AS collection_name, group_concat(DISTINCT st.style SEPARATOR ', ') AS styles, group_concat(DISTINCT sp.equipment SEPARATOR ', ') AS equipment, sp.origin, group_concat(DISTINCT artists.name SEPARATOR ', ') AS artist_names, group_concat(DISTINCT artists.artist_id SEPARATOR ', ') AS artist_ids, location.name AS location_name, p.name AS photographer_name, lc.tgm AS lc_tgm, user.user_name, i.user_id FROM image i LEFT JOIN collection c ON (c.id = i.collection_id) LEFT JOIN (SELECT st.image_id, gs.style, gs.id AS style_id FROM relational_style st LEFT JOIN graffiti_style gs ON (st.style_id = gs.id)) st ON(st.image_id = i.id) LEFT JOIN (SELECT ds.image_id, sp.equipment, b.origin, b.id AS origin_id FROM relational_digital_specs ds LEFT JOIN digital_specs sp ON (ds.digital_specs_id = sp.id) LEFT JOIN born b ON (b.id = sp.born_id)) sp ON (sp.image_id = i.id) LEFT JOIN (SELECT ra.image_id, a.name, a.id AS artist_id FROM relational_artist ra LEFT JOIN artist a ON(ra.artist_id = a.id)) AS artists ON (artists.image_id = i.id) LEFT JOIN location ON (i.city_id = location.id) LEFT JOIN photographer p ON (p.id = i.photographer_id) LEFT JOIN lc_tgm lc ON (lc.id = i.lc_tgm_id) LEFT JOIN user ON (user.id = i.user_id) WHERE ";
        //figure out where clause
        //collections
        if (strlen($collectionQuery) == 0) {
            $query .= " i.collection_id > 0 ";
        } else {
            $arr = explode("|",$collectionQuery);
            $query .= " (";
            for($i = 0; $i < count($arr); ++$i){
                $id = $arr[$i];
                $query .= " i.collection_id = $id or ";
            }
            $query = rtrim($query, ' or ');
            $query .= ") ";
        }
        $query .= "and";
        //styles
        if (strlen($styleQuery) == 0) {
            $query .= " st.style_id > 0 ";
        } else {
            $arr = explode("|",$styleQuery);
            $query .= " (";
            for($i = 0; $i < count($arr); ++$i){
                $id = $arr[$i];
                $query .= " st.style_id = $id or ";
            }
            $query = rtrim($query, ' or ');
            $query .= ") ";
        }
        $query .= "and";
        //medium
        if (strlen($mediumQuery) == 0) {
            $query .= " i.medium_id > 0 ";
        } else {
            $arr = explode("|",$mediumQuery);
            $query .= " (";
            for($i = 0; $i < count($arr); ++$i){
                $id = $arr[$i];
                $query .= " i.medium_id = $id or ";
            }
            $query = rtrim($query, ' or ');
            $query .= ") ";
        }
        $query .= " and ";
        //origin
        if ($originId == 0) {
            $query .= " sp.origin_id > 0 ";
        } else {
            $query .= " sp.origin_id = $originId ";
        }
        $query .= " and ";
        //artist
        if (strlen($artistQuery) == 0) {
            $query .= " artists.artist_id > 0 ";
        }else{
            $arr = explode("|",$artistQuery);
            $query .= " (";
            for($i = 0; $i < count($arr); ++$i){
                $id = $arr[$i];
                $query .= " artists.artist_id = $id or ";
            }
            $query = rtrim($query, ' or ');
            $query .= ") ";
        }
        $query .= " and ";
        if (strlen($locationQuery) == 0) {
            $query .= " location.id > 0 ";
        }else{
            $arr = explode("|",$locationQuery);
            $query .= " (";
            for($i = 0; $i < count($arr); ++$i){
                $id = $arr[$i];
                $query .= " location.id = $id or ";
            }
            $query = rtrim($query, ' or ');
            $query .= ") ";
        }
        $query .= " and ";
        if (strlen($photographerQuery) == 0) {
            $query .= " p.id > 0 ";
        }else{
            $arr = explode("|",$photographerQuery);
            $query .= " (";
            for($i = 0; $i < count($arr); ++$i){
                $id = $arr[$i];
                $query .= " p.id = $id or ";
            }
            $query = rtrim($query, ' or ');
            $query .= ") ";
        }
        $query .= " and ";
        if (strlen($usernameQuery) == 0) {
            $query .= " true ";
            //$query .= " user.id > 0 ";
        }else{
            $arr = explode("|",$usernameQuery);
            $query .= " (";
            for($i = 0; $i < count($arr); ++$i){
                $id = $arr[$i];
                $query .= " user.id = $id or ";
            }
            $query = rtrim($query, ' or ');
            $query .= ") ";
        }

        //group
        $query .= " group by file_name ";
        //limit
        $query .= $limit;

        //run the query
        //echo $query;
        $result = $db->query($query);
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_object()) {
                $imageUrl = Image::get_base_url() . "/images/collection_" .
                    $row->collection_id . "/" . $row->file_name;
                $thumbnailUrl = Image::get_base_url() . "/low_res_images/collection_" .
                    $row->collection_id . "/" . $row->file_name;
                $row->image_url = $imageUrl;
                $row->thumbnail_url = $thumbnailUrl;
                array_push($res->data, $row);
            }
            $res->execution = true;
        }

        //get pagination total
        $pageResult = $db->query("SELECT FOUND_ROWS() as total");
        if ($pageResult->num_rows > 0) {
            $row = $pageResult->fetch_object();
            $res->pages = ceil($row->total / $max);
        }

        return json_encode($res);
    }

    public static function get_image($image_id)
    {

        $res = new \stdClass();
        $res->execution = false;
        $res->data = array();

        $db = new Database();

        $query = "select SQL_CALC_FOUND_ROWS i.id as image_id,i.date_taken, i.file_name, i.title, i.collection_id, c.name as collection_name, group_concat(distinct st.style separator ', ') as styles, group_concat(distinct sp.equipment separator ', ') as equipment, sp.origin, group_concat(distinct artists.name separator ', ') as artist_names, group_concat(distinct artists.artist_id separator ', ') as artist_ids, location.name as location_name, p.name as photographer_name, lc.tgm as lc_tgm, user.user_name, i.user_id from image i left join collection c on (c.id = i.collection_id) left join (select st.image_id, gs.style, gs.id as style_id from relational_style st left join graffiti_style gs on (st.style_id = gs.id)) st on(st.image_id = i.id) left join (select ds.image_id, sp.equipment, b.origin, b.id as origin_id from relational_digital_specs ds left join digital_specs sp on (ds.digital_specs_id = sp.id) left join born b on (b.id = sp.born_id)) sp on (sp.image_id = i.id) left join (select ra.image_id, a.name, a.id as artist_id from relational_artist ra left join artist a on(ra.artist_id = a.id)) as artists on (artists.image_id = i.id) left join location on (i.city_id = location.id) left join photographer p on (p.id = i.photographer_id) left join lc_tgm lc on (lc.id = i.lc_tgm_id) left join user on (user.id = i.user_id) where i.id = $image_id";
        $result = $db->query($query);
        if ($result->num_rows > 0) {
            $row = $result->fetch_object();
            $imageUrl = Image::get_base_url() . "/images/collection_" .
                $row->collection_id . "/" . $row->file_name;
            $thumbnailUrl = Image::get_base_url() . "/low_res_images/collection_" .
                $row->collection_id . "/" . $row->file_name;
            $row->image_url = $imageUrl;
            $row->thumbnail_url = $thumbnailUrl;
            array_push($res->data, $row);
            $res->execution = true;
        }

        return json_encode($res);
    }
}