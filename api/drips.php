<?php
namespace Drips;

if (false) {
	error_reporting(E_ALL);
}

//header('Content-Type: application/json');

//includes
include_once (__DIR__.'/vendor/autoload.php');

use Drips\Image\Image;
use Drips\Search\Search;

//variables
$fn = isset($_REQUEST['fn']) ? $_REQUEST['fn'] : "";
$collectionId = isset($_REQUEST['collection']) ? $_REQUEST['collection'] : "";
$styleId = isset($_REQUEST['style']) ? $_REQUEST['style'] : "";
$mediumId = isset($_REQUEST['medium']) ? $_REQUEST['medium'] : "";
$originId = isset($_REQUEST['origin']) ? $_REQUEST['origin'] : 0;
$page = isset($_REQUEST['page']) ? $_REQUEST['page'] : 1;
$artistQuery = isset($_REQUEST['artist_query']) ? $_REQUEST['artist_query'] : "";
$locationQuery = isset($_REQUEST['location_query']) ? $_REQUEST['location_query'] : "";
$image_id = isset($_REQUEST['image_id']) ? $_REQUEST['image_id'] : "";
$photographerQuery = isset($_REQUEST['photographer_query']) ? $_REQUEST['photographer_query'] : "";
$lctgmQuery = isset($_REQUEST['lc_tgm_query']) ? $_REQUEST['lc_tgm_query'] : "";
$usernameQuery = isset($_REQUEST['user_name_query']) ? $_REQUEST['user_name_query'] : "";

//calls
if($fn == "random_image"){
    echo Image::get_random_image_lowres();
}else if($fn == "get_collections"){
    echo Search::get_collections();
}else if($fn == "get_styles"){
    echo Search::get_styles();
}else if($fn == "get_mediums"){
    echo Search::get_mediums();
}else if($fn == "get_digital_specs"){
    echo Search::get_digital_specs();
}else if($fn == "get_browse_images"){
    //echo Search::browse_images($collectionId, $styleId, $mediumId, $originId, $page,$artistQuery,$locationQuery,$photographerQuery,$lctgmQuery,$usernameQuery);
    echo Search::browse_images_new($collectionId,$styleId,$mediumId,$artistQuery,$locationQuery,$photographerQuery,$usernameQuery,$originId,$page);
}else if($fn == "get_image"){
    echo Search::get_image($image_id);
}else if($fn == "get_artists"){
    echo Search::get_artists();
}else if($fn == "get_locations"){
    echo Search::get_locations();
}else if($fn == "get_photographers"){
    echo Search::get_photographers();
}

?>