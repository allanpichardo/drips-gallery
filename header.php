<?php
//error_reporting(E_ALL);
// Start the session
session_start();
if (isset($_POST['json'])) {
    $_SESSION['json'] = $_POST['json'];
}

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Drips Gallery</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/style.css" rel="stylesheet">
        <link href="css/drips.css" rel="stylesheet">
        <link rel="stylesheet" href="css/justifiedGallery.min.css" />
        <link href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" type="text/css" rel="stylesheet">
        <link rel="stylesheet" href="css/bootstrap-tokenfield.min.css">

        <script type="text/javascript" src="js/jquery.min.js"></script>
        <script type="text/javascript" src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
        <script type="text/javascript" src="js/bootstrap.min.js"></script>
        <script src="js/bootstrap-tokenfield.min.js"></script>
        <script type="text/javascript" src="js/scripts.js"></script>
        <script type="text/javascript" src="js/drips.js"></script>
        <script src="js/jquery.justifiedGallery.js"></script>
        <script src="bower_components/webcomponentsjs/webcomponents-lite.min.js"></script>

        <link rel="import" href="components/drips-browse-category.html">
    </head>

    <body>
        <nav class="header navbar navbar-inverse navbar-static-top">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.php"><span><img src="images/aboutus_photos/logo3.png" style="height:auto;width:84%;margin-top: 5px"></span></a>
                </div>

                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-left">

                        <li <?php
                        echo (basename($_SERVER["REQUEST_URI"], ".php")) === "browse" ? "class=\"active\"" : "";
                        ?>>
                            <a href="browse.php">Discover Drips</a>
                        </li>
                        <li <?php
                        echo (basename($_SERVER["REQUEST_URI"], ".php")) === "about" ? "class=\"active\"" : "";
                        ?>>
                            <a href="about.php">About Us</a>
                        </li>
                    </ul>
                    <!--
                    <ul class="nav navbar-nav navbar-right">
                        <li>
                            <a href="login.php">Login</a>
                        </li>
                        <li>
                            <a href="register.php">Register</a>
                        </li>
                    </ul>
                    -->
                </div>
            </div>
        </nav>
        <div class="container">
