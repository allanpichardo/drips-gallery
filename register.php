<?php include('header.php'); ?>
	<div class="row clearfix">
		<div class="col-md-12 column">
			<div class="page-header">
				<h1>
				Register</h1>
			</div>
		</div>
	</div>
	<div class="row clearfix">
		<div class="col-md-2 column">
		</div>
		<div class="col-md-6 column">
			<form role="form">
				<form class="form-horizontal">
<fieldset>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="firstnameentry">First Name</label>  
  <div class="col-md-4">
  <input id="firstnameentry" name="firstnameentry" type="text" placeholder="" class="form-control input-md" required>
    
  </div>
</div><br><br>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="lastnameentry">Last Name</label>  
  <div class="col-md-4">
  <input id="lastnameentry" name="lastnameentry" type="text" placeholder="" class="form-control input-md" required>
    
  </div>
</div><br><br>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="emailaddressentry">Email Address</label>  
  <div class="col-md-4">
  <input id="emailaddressentry" name="emailaddressentry" type="text" placeholder="" class="form-control input-md" required>
    
  </div>
</div><br><br>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="usernameentry">Username</label>  
  <div class="col-md-4">
  <input id="usernameentry" name="usernameentry" type="text" placeholder="" class="form-control input-md" required>
    
  </div>
</div><br><br>

<!-- Password input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="passwordentry">Password </label>
  <div class="col-md-4">
    <input id="passwordentry" name="passwordentry" type="password" placeholder="" class="form-control input-md" required>
    
  </div>
</div><br><br>

<!-- Password input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="confirmpasswordentry">Confirm Password</label>
  <div class="col-md-4">
    <input id="confirmpasswordentry" name="confirmpasswordentry" type="password" placeholder="" class="form-control input-md" required>
    
  </div>
</div><br><br>

<!-- Button -->
<div class="form-group">
  <label class="col-md-4 control-label" for="submitregisterentry"></label>
  <div class="col-md-4">
    <button id="submitregisterentry" name="submitregisterentry" class="btn btn-primary">Submit</button>
  </div>
</div><br><br>

</fieldset>
</form>
</form>
		</div>
		<div class="col-md-4 column">
		</div>
	</div>
</div>
<?php include('footer.php'); ?>