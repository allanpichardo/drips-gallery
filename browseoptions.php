<?php ?>
<script>

    $(document).ready(function () {

<?php
if (isset($_SESSION['json'])) {
    $json = $_SESSION['json'];
    echo "var json =  '$json';";
    echo 'loadState(json);';
}
?>

        //init gallery
        dripsQuery.getBrowseImages(currentPage);
    });

    $(window).on('beforeunload ', function () {
       saveState();
    });
</script>

<div class="row">
    <div class="col-md-8 col-md-offset-2" style="text-align:center">
        <drips-browse-category class="search-category" id="col" category-name="collections" parameterclick="selected"></drips-browse-category>&nbsp;
        <drips-browse-category class="search-category" id="sty" category-name="styles"></drips-browse-category>&nbsp;
        <drips-browse-category class="search-category" id="med" category-name="mediums"></drips-browse-category>&nbsp;
        <drips-browse-category class="search-category" id="art" category-name="artists"></drips-browse-category>&nbsp;
        <drips-browse-category class="search-category" id="loc" category-name="locations"></drips-browse-category>&nbsp;
        <drips-browse-category class="search-category" id="pho" category-name="photographers"></drips-browse-category>&nbsp;
        <drips-browse-category class="search-category" id="use" category-name="users"></drips-browse-category>&nbsp;
        <br>
    </div>
</div>
        <div style="width:100%;text-align: center">
            <span>
                <form id="tagform" class="form-inline" role="form" style="100%">
                    <br>
                        <div id="tagbox" style="vertical-align: middle;"></div>
                        <!--<input type="text" class="form-control input-medi" id="tokenfield" value=""/>-->
                    <br/>
                    <button style="vertical-align: middle" id="clearbutton" type="button" class="btn btn-danger btn-sm" aria-label="Left Align">
                        <span class="glyphicon glyphicon-remove" aria-hidden="true">Clear</span>
                    </button>
                </form>
            </span>
        </div>


<div id="gallery" class="gallery">

</div>
<br><br>
<div style="height:5%"></div>
