<?php include('header.php'); ?>
<script type="text/javascript">
    $(document).ready(function(){
        getImageInfo(<?php echo $_GET['id'] ?>);
    });
</script>
<div>
    <div class="imagebox">
        
    </div>
    
    <div class="row details">
        <h2 id="title" style="text-align: center"></h2>
        <p>
        <div class="col-md-4" style="text-align: center">
            <span id="artists"></span>
            <br>
            <span id="location-name"></span>
            <br>
            <span id="style-name"></span>
            <br>
            <span id="date-taken"></span>
        </div>
        <div class="col-md-4 col-md-offset-4" style="text-align: center">
            <span id="photographer-name"></span>
            <br>
            <span id="collection-name"></span>
            <br>
            <span id="equipment-name"></span>
            <br>
            <span id="lc-tgm"></span>
        </div>
            
            
        </p>
    </div>
    <br/>
    <br/>
    <br/>
    <br/>
</div>
<?php include('footer.php'); ?>