
$(document).ready(function(){
    $('#tagform').hide();

    dripsQuery = new DripsQuery();

    try {
        document.getElementById('col').addEventListener("parameterclick", function (e) {
            categoryChecked(e);
        });
        document.getElementById('sty').addEventListener("parameterclick", function (e) {
            categoryChecked(e);
        });
        document.getElementById('med').addEventListener("parameterclick", function (e) {
            categoryChecked(e);
        });
        document.getElementById('art').addEventListener("parameterclick", function (e) {
            categoryChecked(e);
        });
        document.getElementById('loc').addEventListener("parameterclick", function (e) {
            categoryChecked(e);
        });
        document.getElementById('pho').addEventListener("parameterclick", function (e) {
            categoryChecked(e);
        });
        document.getElementById('clearbutton').addEventListener("click", function (e) {
            clearAllClicked(e);
        });

        //dripsQuery.getBrowseImages();
    }catch(err){

    }
});

$(window).scroll(function() {
    if($(window).scrollTop() + $(window).height() == ($(document).height())) {
        dripsQuery.getBrowseImages(currentPage+1);
    }
});

totalPages = 0;
currentPage = 1;

var DripsQuery = function(){
    var DripsQuery = {
        collectionTags: '',
        styleTags: '',
        mediumTags: '',
        artistTags: '',
        locationTags: '',
        photographerTags: '',
        collections: new Array(),
        styles: new Array(),
        mediums: new Array(),
        artists: new Array(),
        locations: new Array(),
        photographers: new Array(),
        users: new Array(),
        _trim: function(string){
            return string.replace(/(^[|\s]+)|([|\s]+$)/g, '');
        },
        getAll: function(){
            return new Array().concat(this.collections,this.styles,this.mediums,this.artists,this.locations,this.photographers,this.users);
        },
        clearAll: function(){
            this.collections = new Array();
            this.styles = new Array();
            this.mediums = new Array();
            this.artists = new Array();
            this.locations = new Array();
            this.photographers = new Array();
            this.users = new Array();
        },
        setCollections: function(array){
            this.collections = array;
            var _this = this;
            _this.collectionTags = '';
            for(var i = 0; i < array.length; ++i){
                (function(){
                    _this.collectionTags += array[i].name + ',';
                }())
            }
        },
        setStyles: function(array){
            this.styles = array;
            var _this = this;
            _this.styleTags = '';
            for(var i = 0; i < array.length; ++i){
                (function(){
                    _this.styleTags += array[i].style + ',';
                }())
            }
        },
        setMediums: function(array){
            this.mediums = array;
            var _this = this;
            _this.mediumTags = '';
            for(var i = 0; i < array.length; ++i){
                (function(){
                    _this.mediumTags += array[i].name + ',';
                }())
            }
        },
        setArtists: function(array){
            this.artists = array;
            var _this = this;
            _this.artistTags = '';
            for(var i = 0; i < array.length; ++i){
                (function(){
                    _this.artistTags += array[i].name + ',';
                }())
            }
        },
        setLocations: function(array){
            this.locations = array;
            var _this = this;
            _this.locationTags = '';
            for(var i = 0; i < array.length; ++i){
                (function(){
                    _this.locationTags += array[i].name + ',';
                }())
            }
        },
        setPhotographers: function(array){
            this.photographers = array;
            var _this = this;
            _this.photographerTags = '';
            for(var i = 0; i < array.length; ++i){
                (function(){
                    _this.photographerTags += array[i].name + ',';
                }())
            }
        },
        getTags: function(){
            var tags = this.collectionTags+this.styleTags+this.mediumTags+this.artistTags+this.locationTags+this.photographerTags;
            return tags;
        },
        getQueryUrl: function(){
            var _this = this;
            var baseUrl = "./api/drips.php?fn=get_browse_images";
            var collectionParams = "&collection=";
            var styleParams = "&style=";
            var mediumParams = "&medium=";
            var artistParams = "&artist_query=";
            var locationParams = "&location_query=";
            var photographerParams = "&photographer_query=";

            for(var i = 0; i < this.collections.length; ++i){
                (function(){
                    var id = _this.collections[i].id;
                    collectionParams += id+"|";
                }())
            }
            collectionParams = this._trim(collectionParams);
            for(var i = 0; i < this.styles.length; ++i){
                (function(){
                    var id = _this.styles[i].id;
                    styleParams += id+"|";
                }())
            }
            styleParams = this._trim(styleParams);
            for(var i = 0; i < this.mediums.length; ++i){
                (function(){
                    var id = _this.mediums[i].id;
                    mediumParams += id+"|";
                }())
            }
            mediumParams = this._trim(mediumParams);
            for(var i = 0; i < this.artists.length; ++i){
                (function(){
                    var id = _this.artists[i].id;
                    artistParams += id+"|";
                }())
            }
            artistParams = this._trim(artistParams);
            for(var i = 0; i < this.locations.length; ++i){
                (function(){
                    var id = _this.locations[i].id;
                    locationParams += id+"|";
                }())
            }
            locationParams = this._trim(locationParams);
            for(var i = 0; i < this.photographers.length; ++i){
                (function(){
                    var id = _this.photographers[i].id;
                    photographerParams += id+"|";
                }())
            }
            photographerParams = this._trim(photographerParams);

            baseUrl += collectionParams+styleParams+mediumParams+artistParams+locationParams;
            return baseUrl;
        },
        getBrowseImages: function(page,clear) {
            var _this = this;
            if (page == null) {
                page = 1;
            }
            if (clear == null){
                clear = false;
            }

            currentPage = page;

            var url = _this.getQueryUrl() + "&page=" + page;

            if(clear){
                console.log('clear');
                $('#gallery').empty();
            }

            $.ajax({
                crossDomain: true,
                type: "GET",
                url: url,
                success: function (result){
                    //console.log(result);
                    var json = JSON.parse(result);
                    //console.log(json);
                    if (json.execution) {
                        generateGallery(json);
                    }
                }});
        }
    }
    return DripsQuery;
}

function clearAllClicked(e){
    var cats = document.getElementsByTagName('drips-browse-category');
    for(var i = 0; i < cats.length; ++i){
        cats[i].clearAll();
    }
    $('#tagform').hide();
    dripsQuery.clearAll();
    dripsQuery.getBrowseImages(1,true);
}

function categoryCheckedOld(e){
    if(e.detail){
        //console.log(e.detail);

        switch(e.detail.category){
            case "collections":
                dripsQuery.setCollections(e.detail.selections);
                break;
            case "styles":
                dripsQuery.setStyles(e.detail.selections);
                break;
            case "mediums":
                dripsQuery.setMediums(e.detail.selections);
                break;
            case "artists":
                dripsQuery.setArtists(e.detail.selections);
                break;
            case "locations":
                dripsQuery.setLocations(e.detail.selections);
                break;
            case "photographers":
                dripsQuery.setPhotographers(e.detail.selections);
                break;
        }

        jQuery(function(){
            var tags = dripsQuery.getTags();
            $('#tokenfield').tokenfield();
            if(tags.length > 0){
                $('#tokenfield').value = tags
                $('#tokenfield').tokenfield('setTokens', tags);
                //$('#tagform').show();
            }else{
                $('#tokenfield').tokenfield('destroy');
                document.getElementById('tokenfield').value = '';
                //$('#tagform').hide();
            }
            //console.log(dripsQuery.getTags());
        }())

        dripsQuery.getBrowseImages(1,true);

    }else{
        dripsQuery.clearAll();
        $('#tokenfield').tokenfield();
        $('#tokenfield').tokenfield('destroy');
        document.getElementById('tokenfield').value = '';
        //$('#tagform').hide();
    }

}

function categoryChecked(e){
    if(e.detail){
        //console.log(e.detail);

        var type = 0;

        switch(e.detail.category){
            case "collections":
                dripsQuery.setCollections(e.detail.selections);
                break;
            case "styles":
                type = 1;
                dripsQuery.setStyles(e.detail.selections);
                break;
            case "mediums":
                type = 2;
                dripsQuery.setMediums(e.detail.selections);
                break;
            case "artists":
                dripsQuery.setArtists(e.detail.selections);
                break;
            case "locations":
                dripsQuery.setLocations(e.detail.selections);
                break;
            case "photographers":
                dripsQuery.setPhotographers(e.detail.selections);
                break;
        }

        jQuery(function(){
            var tags = dripsQuery.getAll();
            $('#tagbox').empty();
            for(var i = 0; i < tags.length; ++i){
                var name;
                if(tags[i].name){
                    name = tags[i].name;
                }else if(tags[i].style){
                    name = tags[i].style;
                }else if(tags[i].medium){
                    name = tags[i].medium;
                }
                $('#tagbox').append('<a href="#" class="label label-primary">'+name+'</a>&nbsp;');
            }
            //console.log(dripsQuery.getAll());
            if(tags.length > 0){
                $('#tagform').show();
            }else{
                $('#tagform').hide();
            }
        }())

        dripsQuery.getBrowseImages(1,true);

    }else{
        $('#tagbox').empty();
        $('#tagbox').hide();
    }

}

function randomHomeImage() {
    $.ajax({
        crossDomain: true,
        url: "api/drips.php?fn=random_image",
        success: function (result) {
            //console.log(result);
            var json = JSON.parse(result);
            var url = "";
            url = json.image_url;
            $('#graf').fadeOut("slow", "linear", function () {
                $('#graf').css({"background": "url('" + url + "') 50% 50% no-repeat", "background-size": "cover"});
                $('#graf').fadeIn("slow", "linear", function () {
                    setTimeout(randomHomeImage, 7000);
                });
            });
        }});
}

function extractId(selectedId) {
    return selectedId.substring(selectedId.indexOf("-") + 1);
}

function populateCollections() {

    $.ajax({
        crossDomain: true,
        url: "api/drips.php?fn=get_collections",
        success: function (result) {
            var json = JSON.parse(result);
            //console.log(json);
            var collections = json.data;
            //console.log(collections);
            for (var i = 0; i < collections.length; ++i) {
                var collection = collections[i];
                //append the list item
                $('#ul-collections').append(
                    $('<li>').attr('id', 'collections-' + collection.id).append(
                        $('<a>').attr('href', '#').append(collection.name)));
            }

            //set events
            //dropdown selections
            $(".dropdown-menu li a").click(function () {
                registerSelection($(this).parent().attr("id"));
            });
        }});
}

function generateGallery(json) {

    var images = json.data;

    for (var i = 0; i < images.length; ++i) {
        var image = images[i];
        var tag = "<a href='image.php?id=" + image.image_id + "'><img src='" + image.thumbnail_url + "' alt='" + image.title + "'></a>";
        //console.log(tag);
            $('#gallery').html($('#gallery').html() + tag);

    }

    $('#gallery').justifiedGallery({
        rowHeight: 200,
        lastRow: 'justify',
        maxRowHeight: 300,
        margins: 15,
        fixedHeight: true,
        norewind: (json.page > 1) ? true : false
    });
    //var s = {
    //        rowHeight: 200,
    //        lastRow: 'justify',
    //        maxRowHeight: 300,
    //        margins: 15,
    //        fixedHeight: true,
    //        norewind: true
    //    };
    //$('#gallery').settings = s;
    //$('#gallery').settings.rowHeight = 200;
    //$('#gallery').settings.lastRow = 'justify';
    //$('#gallery').settings.maxRowHeight = 300;
    //$('#gallery').settings.margins = 15;
    //$('#gallery').settings.fixedHeight = true;

    //$('#gallery').justifiedGallery(s);

    //console.log(json);
    currentPage = json.page;
    totalPages = json.pages;
    //generatePagination(json);
}

function getTotalPages() {
    return totalPages;
}

function getCurrentPage() {
    return currentPage;
}

function registerPageClick(id) {
    //console.log(id);
    if (id == "previous") {
        //go to previous page
        //console.log("going to next");
        if (getCurrentPage() > 1) {
            dripsQuery.getBrowseImages(getCurrentPage() - 1);
        }
    } else if (id == "next") {
        //go to next page
        //go to previous page
        //console.log("next");
        if (getCurrentPage() < getTotalPages()) {
            dripsQuery.getBrowseImages(getCurrentPage() + 1);
        }
    } else {
        //a page
        var page = extractId(id);
        //console.log(page);
        dripsQuery.getBrowseImages(page);
    }
}

function generatePagination(json) {
    var list = $(".pagination");
    var prev = '<li><a href="#" id="previous"><span aria-hidden="true">&laquo;</span><span class="sr-only">Previous</span></a></li>';
    var next = '<li><a href="#" id="next"><span aria-hidden="true">&raquo;</span><span class="sr-only">Next</span></a></li>';
    //clear it
    list.html("");
    list.html(list.html() + prev);
    for (var i = 0; i < json.pages; ++i) {
        if (currentPage == i + 1) {
            list.append($("<li>").attr('class', 'active').append($("<a>").attr({href: "#", id: "page-" + (i + 1)}).html("" + (i + 1))));
        } else {
            list.append($("<li>").append($("<a>").attr({href: "#", id: "page-" + (i + 1)}).html("" + (i + 1))));
        }
    }
    list.html(list.html() + next);
    //page events
    $(".pagination li a").click(function () {
        registerPageClick($(this).attr("id"));
    });
}

function getImageInfo(id){

    $.ajax({
        crossDomain: true,
        type: "POST",
        url: "api/drips.php",
        data: {fn: "get_image", image_id: id},
        success: function (result) {
            var json = JSON.parse(result);
            //console.log(json);
            if (json.execution) {
                parseImageInfo(json);
            }
        }});

}

function parseImageInfo(json){

    if(json.execution){
        var image = json.data[0];

        $('.imagebox').append(
            $('<a>').attr('href',image.image_url).append(
                $('<img>').attr('src',image.thumbnail_url)
            ));
        $('#title').html(image.title);
        $('#artists').html('<b>Artist(s): </b><i>'+image.artist_names+'</i>');
        $('#location-name').html('<b>Location: </b><i>'+image.location_name+'</i>');
        $('#photographer-name').html('<b>Photographer: </b><i>'+image.photographer_name+'</i>');
        $('#equipment-name').html('<b>Equipment: </b><i>'+image.equipment+'</i>');
        $('#collection-name').html('<b>Collection: </b><i>'+image.collection_name+'</i>');
        $('#style-name').html('<b>Style(s): </b><i>'+image.styles+'</i>');
        $('#date-taken').html('<b>Date Taken: </b><i>'+image.date_taken+'</i>');
        $('#lc-tgm').html('<b>LC TGM: </b><i>'+image.lc_tgm+'</i>');
    }
}

function saveState(){

    var json = {
        "currentPage":currentPage,
        "totalPages":totalPages,
        "dripsQuery":dripsQuery.stringify()
    };

    $.ajax({
        crossDomain: true,
        type: "POST",
        url: "../header.php",
        data: {json: JSON.stringify(json)},
        success: function (result) {
            console.log("state saved!");
        }});
}

function loadState(json){
    console.log(json);
    json = JSON.parse(json);
    currentPage = json.currentPage;
    totalPages = json.totalPages;

    var query = json.dripsQuery;
    dripsQuery.collections = query.collections;
    dripsQuery.styles = query.styles;
    dripsQuery.mediums = query.mediums;
    dripsQuery.artists = query.artists;
    dripsQuery.locations = query.locations;
    dripsQuery.photographers = query.photographers;
    dripsQuery.users = query.users;
}