<?php include('header.php'); ?>
<div>
	<div class="row clearfix">
		<div class="col-md-12 column">
		  <h1>
				Mission</h1>
			<p>
			<span class="missiontext">The main purpose of Drips Gallery is to capture and preserve graffiti art through a community driven digital archive. We aim to involve the creators and consumers of graffiti in the archival process while increasing search-ability.   This forward-thinking radical archive allows the archivists to further their role from record keeper to proactive facilitator. </span></p>
			<p>
			</p>
		</div>
	</div><br><br>
    <div>
    <h2 class="dripsteam">The Drips Team</h2>
    </div>
	<div class="row clearfix">
		<div class="col-md-8 column"><h2 class="header2v2">Farah Jindani</h2>
			<div>Although a minimilast in most aspects of her life, Farah is interested in the collection, organization, and accessibility of all the interesting things in the world.  Currently, she provides ticketing access as Associate Box Office Manager at the Film Society of Lincoln Center.  She also works as an Archival Intern at CUNY TV and will complete her Masters in Library and Information Science from Queens College at the end of this year. Her hobbies include creative writing, stand-up comedy, and television.
			</div>
	</div>
		<div class="col-md-4 column">
			<img src="images/aboutus_photos/farahphoto.jpg" class="aboutimage">
		</div>
	</div>
	<div class="row clearfix">
		<div class="col-md-8 column">
			<h2 class="header2v2">
				Alexandra Lederman</h2>
			<div>Alexandra is a digital humanitarian and believes in equal access to information. She is interested in capturing culture while it's being created and preserved. Foodways and new, emerging technologies in the library and archive get her ticking!
After 5 years in the hospitality industry, Alexandra finally listened to her true love for books and information. She is currently completing her Masters in Library &amp; Information Science at Queens College. Alexandra is also a Citi Center for Culture x Queens Library Graduate Fellow and is a Library Associate with EdLabs, Teachers College, Columbia University. Alexandra spends her free time with science fiction novels, star gazing, cooking, David Cronenberg films, and vinyls.
</div>
	</div>
		<div class="col-md-4 column">
			<img src="images/aboutus_photos/alexphoto.jpg" class="aboutimage">
		</div>
	</div>
	<div class="row clearfix">
		<div class="col-md-8 column">
			<h2 class="header2v2">
				Allan Pichardo</h2>
			<div>Allan is a software developer for Ride Connect.  He is proficient in Java, Javascript, PHP and MySQL. Developing, programming and designing software and mobile applications is how Allan spends his day. He relaxes by watching television shows set in faraway galaxies. He is also an interactive media artist and a musician. You can check out his music at: <a href="http://mylovemhz.com/" target="new">http://mylovemhz.com/</a>
</div>
	
	  </div>
		<div class="col-md-4 column">
			<img src="images/aboutus_photos/allanphoto.jpg" class="aboutimage">
		</div>
	</div>
   
</div>
<?php include('footer.php'); ?>